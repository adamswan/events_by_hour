view: events_hour {
  sql_table_name: events ;;

  dimension: session_id {
    primary_key: yes
    type: string
    sql: ${TABLE}.session_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      hour,
      date,
      month,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  measure: total_per_hour {
    type: count
  }
}
