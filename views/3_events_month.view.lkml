view: events_month {
  derived_table: {
    explore_source: events_day {
      column: created_date {}
      column: max_per_hour {}
      column: average_per_hour {}
    }
  }

  dimension_group: created {
    type: time
    timeframes: [month,year]
    allow_fill: no
    sql: ${TABLE}.created_date ;;
  }

  dimension: max_total {
    hidden: yes
    type: number
    sql: ${TABLE}.max_per_hour ;;
  }

  dimension: avg_total {
    hidden: yes
    type: number
    sql: ${TABLE}.average_per_hour ;;
  }

  measure: average_per_hour {
    description: "Establishes the average number of events per hour for the selected timeframe"
    type: average
    value_format: "0"
    sql: ${avg_total} ;;
    link: {
      label: "Drill into this month {% if events_month.created_month._in_query %} ({{ created_month }}) {% else %} {% endif %}"
      url: "
      {% assign vis_config = '@{vis_config_for_day}' %}
      {% if events_month.created_month._in_query %}
      /explore/events_by_hour_drilldown/events_day?fields=events_day.created_date,events_day.average_per_hour,events_day.max_per_hour&f[events_day.created_month]={{ created_month | url_encode }}%20for%201%20month&vis_config={{ vis_config | encode_uri }}&sorts=events_day.created_day+asc
      {% else %} {% endif %}
      "
    }
  }

  measure: max_per_hour {
    description: "Establishes the maximum number of events per hour for the selected timeframe"
    type: max
    value_format: "0"
    sql: ${max_total} ;;
    link: {
      label: "Drill into this month {% if events_month.created_month._in_query %} ({{ created_month }}) {% else %} {% endif %}"
      url: "
      {% assign vis_config = '@{vis_config_for_day}'%}
      {% if events_month.created_month._in_query %}
      /explore/events_by_hour_drilldown/events_day?fields=events_day.created_date,events_day.average_per_hour,events_day.max_per_hour&f[events_day.created_month]={{ created_month | url_encode }}%20for%201%20month&vis_config={{ vis_config | encode_uri }}&sorts=events_day.created_day+asc
      {% else %} {% endif %}
      "
    }
  }

  measure: average_per_second {
    description: "Establishes the average number of messages sent per second for the selected timeframe (day or hours)"
    type: average
    value_format: "0"
    sql: ${avg_total} ;;
    link: {
      icon_url: "https://mgage.com/wp-content/uploads/2019/08/sr-icons-_0003_activity-rate-monitor.png"
      label: "Drill into this day
      {% if throughput_day.period_date._in_query and throughput_day.carrier._in_query %} ({{ period_date }}) for {{ carrier }}
      {% elsif throughput_day.period_date._in_query %}  ({{ period_date }}) {% else %} {% endif %}"
      url: "
      {% assign vis_config = '{
      \"type\"  : \"looker_area\",
      \"point_style\" : \"circle_outline\",
      \"series_types\" : {
      \"throughput_hour.average_per_second\" : \"area\",
      \"throughput_hour.max_per_second\" : \"area\",
      \"throughput_hour.total_max_cap\" : \"line\"
      },
      \"series_colors\" : {
      \"throughput_hour.average_per_second\" : \"#116e7e\",
      \"throughput_hour.max_per_second\" : \"#3ca286\",
      \"throughput_hour.total_max_cap\" : \"#b21623\"
      },
      \"series_point_styles\" : {
      \"throughput_hour.average_per_second\" : \"circle\",
      \"throughput_hour.max_per_second\" : \"circle\",
      \"throughput_hour.total_max_cap\" : \"diamond\"
      }
      }'
      %}
      {% if throughput_day.period_date._in_query and throughput_day.carrier._in_query %}
      /explore/throughput/throughput_hour?fields=throughput_hour.period_hour,throughput_hour.average_per_second,throughput_hour.max_per_second,throughput_hour.total_max_cap&f[throughput_hour.period_date]={{ period_date | url_encode }}%20for%201%20day&f[throughput_hour.carrier]={{ _filters['carrier'] | url_encode }}&f[throughput_parameters.cap_per_second]={{ _filters['throughput_parameters.cap_per_second'] | url_encode }}&vis_config={{ vis_config | url_encode }}&sorts=throughput_hour.period_hour+asc
      {% elsif throughput_day.period_date._in_query %}
      /explore/throughput/throughput_hour?fields=throughput_hour.period_hour,throughput_hour.average_per_second,throughput_hour.max_per_second,throughput_hour.total_max_cap&f[throughput_hour.period_date]={{ period_date | url_encode }}%20for%201%20day&f[throughput_parameters.cap_per_second]={{ _filters['throughput_parameters.cap_per_second'] | url_encode }}&vis_config={{ vis_config | url_encode }}&sorts=throughput_hour.period_hour+asc
      {% else %} {% endif %}"
    }
  }

}
